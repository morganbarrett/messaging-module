<div id="messagesContainer">
	<div class="col-md-4 col-md-offset-2">
		<div class="panel panel-default panel-heading-fullwidth panel-primary">
			<div class="panel-heading">
				<div class="tools">
					<a href="#">
						<span class="icon s7-edit"></span>
					</a>
					<a href="#">
						<function call="icon">remove-circle</function>
					</a>
				</div>
				
				<div class="title">Contact List</div>
			</div>
	
			<div class="chat chat-scroll">
				<div class="chat-contacts">
					<div class="am-scroller nano">
						<div class="content nano-content">
							<div class="chat-category">Family</div>
							
							<div class="chat-contact-list">
								<div class="user">
									<a href="#">
										<img src="assets/img/avatar4.jpg">
										<div class="user-data">
											<span class="status away"></span>
											<span class="name">Claire Sassu</span>
											<span class="message">Can you share the...</span>
										</div>
									</a>
								</div>
								
								<div class="user">
									<a href="#">
										<img src="assets/img/avatar7.jpg">
										
										<div class="user-data">
											<span class="status"></span>
											<span class="name">Maggie jackson</span>
											<span class="message">I confirmed the info.</span>
										</div>
									</a>
								</div>
								
								<div class="user">
									<a href="#">
										<img src="assets/img/avatar3.jpg">
										<div class="user-data">
											<span class="status offline"></span>
											<span class="name">Joel Kingspan>
											<span class="message">Ready for the meeti...</span>
										</div>
									</a>
								</div>
							</div>
				
							<div class="chat-category">Friends</div>
							
							<div class="chat-contact-list">
								<div class="user">
									<a href="#">
										<img src="assets/img/avatar6.jpg">
										<div class="user-data2">
											<span class="status"></span>
											<span class="name">Mike Bolthort</span>
										</div>
									</a>
								</div>
				
								<div class="user">
									<a href="#">
										<img src="assets/img/avatar7.jpg">
										<div class="user-data2">
											<span class="status"></span>
											<span class="name">Maggie jackson</span>
										</div>
									</a>
								</div>
				
								<div class="user">
									<a href="#">
										<img src="assets/img/avatar8.jpg">
										<div class="user-data2">
											<span class="status offline"></span>
											<span class="name">Jhon Voltemar</span>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="chat-search">
				<input type="text" placeholder="Search..." name="q">
				<span class="s7-search"></span>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-default panel-heading-fullwidth panel-primary">
			<div class="panel-heading"> 
				<div class="tools">
					<a href="#">
						<span class="icon s7-edit"></span>
					</a>

					<a href="#">
						<span class="icon s7-close"></span>
					</a>
				</div>
					
				<div class="title">Chat Messages</div>
			</div>

			<div class="chat">
				<div class="chat-window">
					<div class="title">
						<div class="user">
							<img src="assets/img/avatar7.jpg">
							<h2 class="user-name">Maggie jackson</h2>
							<span>Active 1h ago</span>
						</div>
						
						<span class="icon return s7-angle-left"></span>
					</div>
					
					<div class="chat-messages">
						<ul>
							<li class="friend">
							<div class="msg">Hello</div>
							</li>
							<li class="self">
							<div class="msg">Hi, how are you?</div>
							</li>
							<li class="friend">
							<div class="msg">Good, I'll need support with my pc</div>
							</li>
							<li class="self">
							<div class="msg">Sure, just tell me what is going on with your computer?</div>
							</li>
							<li class="friend">
							<div class="msg">I don't know it just turns off suddenly</div>
							</li>
							<li class="self">
							<div class="msg">Ok, we'll help you with this issue</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="chat-input">
				<div class="input-wrapper">
					<span class="photo s7-camera"></span>
					<input type="text" placeholder="Message..." name="q" autocomplete="off">
					<span class="send-msg s7-paper-plane"></span>
				</div>
			</div>
		</div>
	</div>
</div>
